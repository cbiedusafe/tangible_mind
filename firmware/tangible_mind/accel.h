#ifndef TANGIBLE_MIND_ACCEL_H
#define TANGIBLE_MIND_ACCEL_H

#include <Arduino.h>

void accel_setup();
void accel_get_data(int *xyz);

#endif
