#ifndef TANGIBLE_MIND_DISPLAY_H
#define TANGIBLE_MIND_DISPLAY_H

#include <Arduino.h>

void display_setup();
void display_loop();
void display_show_form(int id);
void display_set_volume(int volume);
void display_play_sound(int id);
void display_event_handler(void);
void display_debug_msg(char *msg);
void display_callback(uint8_t *data, size_t len, uint16_t from);

#endif
