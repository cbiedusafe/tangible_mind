#ifndef TANGIBLE_MIND_BUTTON_H
#define TANGIBLE_MIND_BUTTON_H

#include <Arduino.h>

void button_setup();
void button_loop();
uint8_t button_poll();

#endif
