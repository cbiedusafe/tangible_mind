#include "xb.h"
#include "config.h"

#include <XBee.h>

// Make sure there is enough space in these arrays for all the callbacks!
const int XbMaxCallbacks = 16;
char xbIds[XbMaxCallbacks] = {0};
XbCallback xbCallbacks[XbMaxCallbacks] = {0};
// Never increment past XbMaxCallbacks!
int xbCallbackIndex = 0;

XBee xb = XBee();
XBeeResponse xbResponse = XBeeResponse();
Rx16Response rx16 = Rx16Response();

void xb_setup() 
{ 
  Serial1.begin(9600);
  xb.setSerial(Serial1);
}

const int MsgSize = 32;
char msg[MsgSize + 1] = {0}; // One extra for terminating character.
int msgI = 0;
char buf[12]; // "-2147483648\0"

void xb_loop() 
{ 
  xb.readPacket();
  if(xb.getResponse().isAvailable())
  {
    if(xb.getResponse().getApiId() == RX_16_RESPONSE)
    {
      xb.getResponse().getRx16Response(rx16);

      #ifdef TANGIBLE_MIND_DEBUG_XB
      Serial.print("XB remote address: ");
      Serial.println(rx16.getRemoteAddress16());  
      Serial.print("XB data length: ");
      Serial.println(rx16.getDataLength());    
      Serial.println((char*)rx16.getData());
      Serial.print("XB signal strength: ");
      Serial.println(rx16.getRssi());
      #endif
      
      xb_got_data(rx16.getData(), rx16.getDataLength(), rx16.getRemoteAddress16());
    }
    else
    {
      #ifdef TANGIBLE_MIND_DEBUG_XB
      String s = "XB api ";
      s += itoa(xb.getResponse().getApiId(), buf, 10);
      s.toCharArray(msg, MsgSize);
      display_debug_msg(msg);
      #endif
    }
  }
  else if(xb.getResponse().isError())
  {
    #ifdef TANGIBLE_MIND_DEBUG_XB
    String s = "XB error ";
    s += itoa(xb.getResponse().getErrorCode(), buf, 10);
    s.toCharArray(msg, MsgSize);
    display_debug_msg(msg);
    #endif
  }
}

void xb_got_data(uint8_t* data, size_t len, uint16_t from)
{
  if(len > 0)
  {
    for(uint8_t i = 0; i < xbCallbackIndex; ++i) // Loop through the amount of set callbacks
    {
      if(xbIds[i] == data[len - 1])
      {
        (*xbCallbacks[i])(data, len, from);
        return;
      }
    }
  }
 
  #ifdef TANGIBLE_MIND_DEBUG_XB
  Serial.print("XB parse error: ");
  Serial.println((char*)data); // Possible buffer overflow!
  #endif
}

void xb_send_data(uint8_t *payload, size_t len)
{
  #ifdef TANGIBLE_MIND_DEBUG_XB
  Serial.print("XB sending ");
  Serial.print(len);
  Serial.println(" bytes");
  #endif  
  
  Tx16Request tx = Tx16Request(0xFFFF, payload, len);
  xb.send(tx);
}

void xb_register_callback(char id, XbCallback func)
{
  if(xbCallbackIndex >= XbMaxCallbacks)
  {
    #ifdef TANGIBLE_MIND_DEBUG_XB
    Serial.println("XB callback overflow");
    #endif
    
    return;
  }
  
  xbIds[xbCallbackIndex] = id;
  xbCallbacks[xbCallbackIndex] = func;
   
  ++xbCallbackIndex;
}

