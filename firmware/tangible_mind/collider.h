#ifndef TANGIBLE_MIND_COLLIDER_H
#define TANGIBLE_MIND_COLLIDER_H

#include <Arduino.h>

struct ColliderEvent
{
  int force[3];
  char id;
};

void collider_setup();
void collider_loop(int *acceleration);
void collider_callback(uint8_t *data, size_t len, uint16_t from);
void collider_check();

#endif
