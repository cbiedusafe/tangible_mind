#include "display.h"
#include "config.h"

#include <AltSoftSerial.h>
#include <genieArduino.h>

const unsigned long DisplayMinMsBetweenSounds = 1000;

AltSoftSerial Serial4D;
unsigned long displaySoundPlayMs = 0;

void display_setup() 
{ 
  Serial4D.begin(9600);
 
  genieBegin(Serial4D);
  genieAttachEventHandler(display_event_handler);
  xb_register_callback('F', display_callback);
  xb_register_callback('S', display_callback);
}

void display_loop() 
{ 
  genieDoEvents();
}

void display_show_form(int id)
{
  genieWriteObject(GENIE_OBJ_FORM, id, 0);
}

void display_set_volume(int volume)
{
  genieWriteObject(GENIE_OBJ_SOUND, 0x01, volume);
}

void display_play_sound(int id)
{
  unsigned long ms = millis();
  if(ms - displaySoundPlayMs > DisplayMinMsBetweenSounds)
  {
    genieWriteObject(GENIE_OBJ_SOUND, 0x04, 0);   
    genieWriteObject(GENIE_OBJ_SOUND, 0x00, id);
    displaySoundPlayMs = ms;
  }
}

void display_event_handler(void) 
{
  genieFrame Event;
  genieDequeueEvent(&Event);

  /*for(int index = 0; index < 6; ++index)
  {
    if (genieEventIs(&Event, GENIE_REPORT_EVENT, GENIE_OBJ_WINBUTTON, index) ||
      genieEventIs(&Event, GENIE_REPORT_OBJ,  GENIE_OBJ_WINBUTTON, index)) 
    {
      Serial.print(index);
    }
  }*/
}

void display_debug_msg(char *msg)
{
  #ifdef TANGIBLE_MIND_DEBUG
  genieWriteStr(0, msg);
  #endif
}

void display_callback(uint8_t *data, size_t len, uint16_t from)
{
  #ifdef TANGIBLE_MIND_DEBUG_DISPLAY
  Serial.println ("Display callback");
  #endif
  
  if(len >= 2)
  {
    int id = char(data[0]) - '0';
    char func = char(data[1]);
    if(func == 'F')
      display_show_form(id);
    else if(func == 'S')
      display_play_sound(id);
  }
}
