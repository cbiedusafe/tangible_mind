#include "button.h"
#include "vibra.h"
#include "config.h"

const int buttonPin = 0;    // the number of the pushbutton pin

int buttonOutputState = 1;         // the current state of the output pin
int buttonState;             // the current reading from the input pin
int lastButtonState = 0;   // the previous reading from the input pin

// the following variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 50;    // the debounce time; increase if the output flickers

uint8_t buttonEvent = 'B';

void button_setup()
{
  pinMode(buttonPin, INPUT);
}

void button_loop()
{
  if(button_poll())
  {
    #ifdef TANGIBLE_MIND_DEBUG_BUTTON
    Serial.println("Button pushed");
    #endif
    vibra_effect(2);
    led_effect(2);
    xb_send_data(&buttonEvent, sizeof(buttonEvent));
  }
}

uint8_t button_poll() {
  // read the state of the switch into a local variable:
  int reading = digitalRead(buttonPin);

  // check to see if you just pressed the button 
  // (i.e. the input went from LOW to HIGH),  and you've waited 
  // long enough since the last press to ignore any noise:  

  // If the switch changed, due to noise or pressing:
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  } 
  
  if ((millis() - lastDebounceTime) > debounceDelay)
  {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      // only toggle the LED if the new button state is HIGH
      if (buttonState == 1) {
        return 1;
      }
    }
  }

  // save the reading.  Next time through the loop,
  // it'll be the lastButtonState:
  lastButtonState = reading;
  return 0;
}

