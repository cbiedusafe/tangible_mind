#include "led.h"
#include "config.h"

#include "xb.h"

const uint8_t ledPinR = 12, ledPinG = 14, ledPinB = 15;

// Effect start time compared with millis()
unsigned long ledEffectStart = 0;
// Number of RGB slots
const int LedEffectSize = 10;
// Each slot represents 100 ms
const int LedSlotMillis = 50;
// Storage for RGB values
uint8_t ledEffectSlots[LedEffectSize * 3] = {0};
char ledEffectQueued = 0;

void led_setup()
{
  pinMode(ledPinR, OUTPUT);
  pinMode(ledPinG, OUTPUT);
  pinMode(ledPinB, OUTPUT);
  digitalWrite(ledPinR, 1);
  delay(200);
  digitalWrite(ledPinG, 1);
  delay(200);
  digitalWrite(ledPinB, 1);
  delay(200);
  digitalWrite(ledPinR, 0);
  delay(200);
  digitalWrite(ledPinG, 0);
  delay(200);
  digitalWrite(ledPinB, 0);
  
  xb_register_callback('L', led_callback);
}

void led_loop()
{
  if(ledEffectQueued == 0)
    return;
    
  unsigned long currentMs = millis();
    
  // Get effect slot index.
  unsigned long index = (currentMs - ledEffectStart) / LedSlotMillis; 
  if(currentMs < ledEffectStart)
  {
    //led_set_rgb(0, 0, 0);
  }
  else if(index >= LedEffectSize)
  {
    //led_set_rgb(0, 0, 0);
    ledEffectQueued = 0;
  }
  else
  {
    int index3 = index * 3;
    led_set_rgb(ledEffectSlots[index3], ledEffectSlots[index3 + 1], ledEffectSlots[index3 + 2]);
  }
}

void led_effect(int effect)
{
  #ifdef TANGIBLE_MIND_DEBUG_LED
  Serial.print("LED effect ");
  Serial.println(effect);
  #endif
  
  if(effect == 1)
  {
    // Set color pattern
    for(int i = 0; i < LedEffectSize; ++i)
    {
      int i3 = 3 * i;
      ledEffectSlots[i3] = ((i + 1) % 2) * 0xFF;
      ledEffectSlots[i3 + 1] = ((i + 1) % 3) * 0xFF;
      ledEffectSlots[i3 + 2] = ((i + 1) % 4) * 0xFF;
    }
  }
  else if(effect == 2)
  {
    memset(ledEffectSlots, 0, LedEffectSize);
    ledEffectSlots[0] = 0xFF;
    ledEffectSlots[1] = 0xFF;
    ledEffectSlots[2] = 0xFF;
  }
  else
  {
    #ifdef TANGIBLE_MIND_DEBUG_LED
    Serial.print("LED error: undefined effect");
    #endif
    
    return;
  }
  
  // Queue effect
  ledEffectStart = millis();
  ledEffectQueued = 1;
}

void led_set_rgb(uint8_t r, uint8_t g, uint8_t b)
{
  analogWrite(ledPinR, r);
  analogWrite(ledPinG, g);
  analogWrite(ledPinB, b);
}

void led_set_rgb(float r, float g, float b)
{
  led_set_rgb(uint8_t(r * 255), uint8_t(g * 255), uint8_t(b * 255));
}


void led_set_hsv(float h, float s, float v)
{
  int i;
  float f, p, q, t;
  if( s == 0 )
  {
    led_set_rgb(v, v, v);
    return;
  }
  
  h /= 60;			// sector 0 to 5
  i = floor(h);
  f = h - i;			// factorial part of h
  p = v * (1 - s);
  q = v * (1 - s * f);
  t = v * (1 - s * ( 1 - f ));
  
  switch(i)
  {
    case 0:
      led_set_rgb(v, t, p);  
      break;
    
    case 1:
      led_set_rgb(q, v, p);
      break;
      
    case 2:
      led_set_rgb(p, v, t);
      break;
    
    case 3:
      led_set_rgb(p, q, v);
      break;

    case 4:
      led_set_rgb(t, p, v);
      break;

    default:
      led_set_rgb(v, p, q);
      break;
    }
}

void led_callback(uint8_t *data, size_t len, uint16_t from)
{
  #ifdef TANGIBLE_MIND_DEBUG_LED
  Serial.println ("LED callback");
  #endif
  
  if(len >= 2)
  {
    int effect = char(data[0]) - '0';
    led_effect(effect);
  }
}

char led_is_effect_queued()
{
  return ledEffectQueued;
}
