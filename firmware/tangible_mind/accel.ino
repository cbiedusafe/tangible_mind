
//Arduino 1.0+ Only!
//Arduino 1.0+ Only!
#include <Wire.h>
#include <ADXL345.h>
ADXL345 adxl;
//variable adxl is an instance of the ADXL345 library

void accel_setup(){
  #ifdef TANGIBLE_MIND_NO_ACCEL
  return;
  #endif
  
  adxl.powerOn();
  //set activity/ inactivity thresholds (0-255)
  adxl.setActivityThreshold(75);
  //62.5mg per increment
  adxl.setInactivityThreshold(75);
  //62.5mg per increment
  adxl.setTimeInactivity(10);
  // how many seconds of no activity is inactive?
  //look of activity movement on this axes-1 == on; 0 == off
  adxl.setActivityX(1);
  adxl.setActivityY(1);
  adxl.setActivityZ(1);
  //look of inactivity movement on this axes-1 == on; 0 == off
  adxl.setInactivityX(1);
  adxl.setInactivityY(1);
  adxl.setInactivityZ(1);
  //look of tap movement on this axes-1 == on; 0 == off
  adxl.setTapDetectionOnX(0);
  adxl.setTapDetectionOnY(0);
  adxl.setTapDetectionOnZ(1);
  //set values for what is a tap, and what is a double tap (0-255)
  adxl.setTapThreshold(50);
  //62.5mg per increment
  adxl.setTapDuration(15);
  //625μs per increment
  adxl.setDoubleTapLatency(80);
  //1.25ms per increment
  adxl.setDoubleTapWindow(200);
  //1.25ms per increment
  //set values for what is considered freefall (0-255)
  adxl.setFreeFallThreshold(7);
  //(5-9) recommended-62.5mg per increment
  adxl.setFreeFallDuration(45);
  //(20-70)recommended-5ms per increment
  //setting all interupts to take place on int pin 1
  //I had issues with int pin 2, was unable to reset it
  adxl.setInterruptMapping(ADXL345_INT_SINGLE_TAP_BIT, ADXL345_INT1_PIN);
  adxl.setInterruptMapping(ADXL345_INT_DOUBLE_TAP_BIT, ADXL345_INT1_PIN);
  adxl.setInterruptMapping(ADXL345_INT_FREE_FALL_BIT, ADXL345_INT1_PIN);
  adxl.setInterruptMapping(ADXL345_INT_ACTIVITY_BIT, ADXL345_INT1_PIN);
  adxl.setInterruptMapping(ADXL345_INT_INACTIVITY_BIT,ADXL345_INT1_PIN);
  //register interupt actions-1 == on; 0 == off
  adxl.setInterrupt(ADXL345_INT_SINGLE_TAP_BIT,1);
  adxl.setInterrupt(ADXL345_INT_DOUBLE_TAP_BIT,1);
  adxl.setInterrupt(ADXL345_INT_FREE_FALL_BIT,1);
  adxl.setInterrupt(ADXL345_INT_ACTIVITY_BIT,1);
  adxl.setInterrupt(ADXL345_INT_INACTIVITY_BIT,1);
}

void accel_get_data(int *xyz)
{
  #ifdef TANGIBLE_MIND_NO_ACCEL
  return;
  #endif  
  
//Boring accelerometer stuff
adxl.readAccel(xyz);

#ifdef TANGIBLE_MIND_DEBUG_ACCEL
Serial.print("Accel:");
for(uint8_t i = 0; i < 3; ++i)
{
  Serial.print(' ');
  Serial.print(xyz[i]); 
}
Serial.print('\n');
#endif
//read the accelerometer values and store them in variables x,y,z
}

