#include "collider.h"
#include "config.h"

#include "xb.h"
#include "accel.h"
#include "vibra.h"
#include "led.h"

// Axis that is facing forward. X = 0, Y = 1, Z = 2
const uint8_t ColliderForwardAxis = 0;
// The treshold force for detecting collision
const int CollisionTresholdForce = -100;
// Maximum allowed time difference in microseconds
const uint32_t CollisionTresholdMicros = 10000;
// For checking that movement only occurred roughly in forward direction
const int CollisionThresholdOtherAxis = 50;

// Acceleration history
int colliderPrevAcceleration[3] = {0};
// Current force
int colliderForce[3] = {0};

uint32_t colliderMicros = 0, incomingColliderMicros = 0;

// Network data packets
ColliderEvent colliderEvent = {0}, incomingColliderEvent = {0};

void collider_setup()
{
  colliderEvent.id = 'C';
  xb_register_callback(colliderEvent.id, collider_callback);
}

void collider_loop(int *acceleration)
{  
  uint8_t noCollision = 0;
  
  for(uint8_t i = 0; i < 3; ++i)
  {
    colliderForce[i] = acceleration[i] - colliderPrevAcceleration[i];
    if(i != ColliderForwardAxis)
    {
      if(abs(colliderForce[i]) > CollisionThresholdOtherAxis)
        noCollision = 1;
    }
  }
  
  if(colliderForce[ColliderForwardAxis] < CollisionTresholdForce && noCollision == 0)
  {
    uint32_t currentMicros = micros();
    if(currentMicros - colliderMicros  > 10000) // Prevent sending packets out too tightly
    {
      xb_send_data((uint8_t*)&colliderEvent, sizeof(ColliderEvent));
    }
    colliderMicros = currentMicros;
    collider_check();
    
    #ifdef TANGIBLE_MIND_DEBUG_COLLIDER
    Serial.print("Collision: ");
    for(uint8_t i = 0; i < 3; ++i)
    {
      Serial.print(' ');
      Serial.print(colliderForce[i]);
    }
    Serial.print('\n');
    #endif
  }
    
  for(uint8_t i = 0; i < 3; ++i)
    colliderPrevAcceleration[i] = acceleration[i];
}

void collider_callback(uint8_t *data, size_t len, uint16_t from)
{
  incomingColliderMicros = micros(); 
  memcpy(&incomingColliderEvent, data, len);
  collider_check();
}

void collider_check()
{
  uint32_t diffMicros = 0;
  if(colliderMicros > incomingColliderMicros)
  {
    diffMicros = colliderMicros - incomingColliderMicros;
  }
  else
  {
    diffMicros = incomingColliderMicros - colliderMicros;
  }
  
  // Yay we have a collision
  if(diffMicros < CollisionTresholdMicros)
  {
    vibra_effect(1);
    display_show_form(2);
    display_play_sound(0);
    led_effect(1);
  }
}
