#ifndef TANGIBLE_MIND_XB_H
#define TANGIBLE_MIND_XB_H

#include <Arduino.h>

typedef void (*XbCallback)(uint8_t* data, size_t len, uint16_t from);

void xb_setup();
void xb_loop();
void xb_got_data(uint8_t *data, size_t len, uint16_t from);
void xb_send_data(uint8_t *payload, size_t len);
void xb_register_callback(char id, XbCallback func);

#endif


