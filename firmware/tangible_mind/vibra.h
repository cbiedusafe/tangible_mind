#ifndef TANGIBLE_MIND_VIBRA_H
#define TANGIBLE_MIND_VIBRA_H

#include <Arduino.h>

void vibra_setup();
void vibra_loop();
void vibra_effect(int effect);
void vibra_callback(uint8_t *data, size_t len, uint16_t from);

#endif


