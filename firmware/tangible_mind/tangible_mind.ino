#include "display.h"
#include "xb.h"
#include "vibra.h"
#include "led.h"
#include "collider.h"
#include "config.h"

int resetPin = 11;
int resetPin2 = 13;

void reset()
{
  digitalWrite(resetPin, 0);
  digitalWrite(resetPin2, 0);
  delay(100);
  digitalWrite(resetPin, 1);
  digitalWrite(resetPin2, 1); 
  delay(3500);
}

void setup() 
{ 
  pinMode(resetPin, OUTPUT);
  pinMode(resetPin2, OUTPUT);
  digitalWrite(resetPin, 1);
  digitalWrite(resetPin2, 1);
  
  Serial.begin(9600);
  Serial.println("Tangible Mind built on " __DATE__ " at " __TIME__ "\n");
  
  Serial.println("Display");
  display_setup();
  Serial.println("XB");
  xb_setup();
  Serial.println("Vibra");
  vibra_setup();
  Serial.println("Accel");
  accel_setup();
  Serial.println("Collider");
  collider_setup();
  Serial.println("Reset");
  reset();
  
  Serial.println("Volume 100");
  display_set_volume(100);
  
  Serial.println("LED");
  led_setup();
  
  Serial.println("Ready");
}

struct GyroEvent
{
  char id;
  char dir;
};
GyroEvent gyroEvent = {'G', '0'};

unsigned long lastMs = 0;
unsigned long lastLedMs = 0;
float hue = 0, sat = 1;
int acceleration[3] = {0};

void change_hue(float diff)
{
  if(diff > 120)
    diff = 120;
  else if(diff < -120)
    diff = -120;
   
  hue += diff;
  if(hue >= 360)
    hue -= 360;
  else if(hue < 0)
    hue += 360;
}

void change_sat(float diff)
{
  if(diff > 0.3f)
    diff = 0.3f;
  else if(diff < -0.3f)
    diff = -0.3f;
   
  sat += diff;
  if(sat > 1)
    sat = 1;
  else if(sat < 0)
    sat = 0;
}

char tiltSoundPlaying = 0;
void play_tilt_sound()
{
  if(!tiltSoundPlaying)
  {
    display_play_sound(0);
    tiltSoundPlaying = 1;
  }
}

char imageChanged = 0;
void change_image(int id)
{
  if(!imageChanged)
  {
    display_show_form(id);
    imageChanged = 1;
  }  
}

void loop() 
{ 
  unsigned long currentMs = millis();

  #ifdef TANGIBLE_MIND_NO_ACCEL
  // Autorotate colors
  if(currentMs - lastLedMs > 50)
  {
    lastLedMs = currentMs;
    led_set_hsv(hue, 1, 1);
    if(++hue >= 360)
      hue = 0;
  }
  #endif

  xb_loop();
  display_loop();
  vibra_loop();
  button_loop();
  
  accel_get_data(acceleration);
  collider_loop(acceleration);
  
  if(acceleration[0] > 100)
  {
    change_sat((acceleration[0] - 100) / 100000.0f);
    change_image(0);
  }
  else if(acceleration[0] < -100)
  {
    change_sat((acceleration[0] + 100) / 100000.0f);
    change_image(1);
  }
  else
  {
    imageChanged = 0;
  }
  
  if(acceleration[1] > 100)
  {
    change_hue((acceleration[1] - 100) / 1000.0f);
    play_tilt_sound();
  }
  else if(acceleration[1] < -100)
  {
    change_hue((acceleration[1] + 100) / 1000.0f);
    play_tilt_sound();
  }
  else
  {
    tiltSoundPlaying = 0;
  }
  
  if(!led_is_effect_queued())
    led_set_hsv(hue, sat, 1);
    
  led_loop();
}
