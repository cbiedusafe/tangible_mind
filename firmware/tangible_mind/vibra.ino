#include "vibra.h"
#include "config.h"

#include "xb.h"

int vibraPin = 21;
int vibraPin2 = 20;

// Effect start time compared with millis()
unsigned long vibraEffectStart = 0;
// Number of ON/OFF slots
const int VibraEffectSize = 10;
// Each slot represents 100 ms
const int VibraSlotMillis = 100;
// Storage for ON/OFF values
char vibraEffectSlots[VibraEffectSize] = {0};
char vibraEffectQueued = 0;

void vibra_setup()
{
  pinMode(vibraPin, OUTPUT);
  pinMode(vibraPin2, OUTPUT);
  
  vibra_set(1);
  delay(10);
  vibra_set(0);
  
  xb_register_callback('V', vibra_callback);
}

void vibra_loop()
{
  if(vibraEffectQueued == 0)
    return;
    
  unsigned long currentMs = millis();
    
  // Get effect slot index.
  unsigned long index = (currentMs - vibraEffectStart) / VibraSlotMillis; 
  if(currentMs < vibraEffectStart)
  {
    vibra_set(0);
  }
  else if(index >= VibraEffectSize)
  {
    vibra_set(0);
    vibraEffectQueued = 0;
  }
  else
  {
    vibra_set(vibraEffectSlots[index]);
  }
}

void vibra_set(unsigned char enabled)
{
   digitalWrite(vibraPin, enabled);
   digitalWrite(vibraPin2, enabled);
}

void vibra_effect(int effect)
{
  #ifdef TANGIBLE_MIND_DEBUG_VIBRA
  Serial.print("Vibra effect ");
  Serial.println(effect);
  #endif
  
  if(effect == 1)
  {
    // Set vibration pattern
    for(int i = 0; i < VibraEffectSize; ++i)
    {
      vibraEffectSlots[i] = (i + 1) % 2;
    }
  }
  else if(effect == 2)
  {
    memset(vibraEffectSlots, 0, VibraEffectSize);
    vibraEffectSlots[0] = 1;
  }
  else
  {
    #ifdef TANGIBLE_MIND_DEBUG_VIBRA
    Serial.print("Vibra error: undefined effect");
    #endif
    
    return;
  }
  
  // Queue effect
  vibraEffectStart = millis();
  vibraEffectQueued = 1;
}

void vibra_callback(uint8_t *data, size_t len, uint16_t from)
{
  #ifdef TANGIBLE_MIND_DEBUG_VIBRA
  Serial.println ("Vibra callback");
  #endif
  
  if(len >= 2)
  {
    int effect = char(data[0]) - '0';
    vibra_effect(effect);
  }
}

