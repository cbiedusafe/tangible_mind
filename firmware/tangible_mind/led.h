#ifndef TANGIBLE_MIND_LED_H
#define TANGIBLE_MIND_LED_H

#include <Arduino.h>

void led_setup();
void led_loop();
void led_effect(int effect);
void led_set_rgb(uint8_t r, uint8_t g, uint8_t b);
void led_set_rgb(float r, float g, float b);
void led_set_hsv(float h, float s, float v);
void led_callback(uint8_t *data, size_t len, uint16_t from);
char led_is_effect_queued();

#endif
