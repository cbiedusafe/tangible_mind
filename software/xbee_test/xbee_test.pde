import processing.serial.*;

void setup() {
  size(200, 200);

  println("Available serial ports:");
  println(Serial.list());
  devicesSetup("COM18");
}

int n = 0;
void draw() {
  int m = ++n % 800;
  
  if(m == 0) {
    devicesVibrate(0xFF, 0xFF, 1);
  }
  else if(m == 100) {
    devicesLED(0xFF, 0xFF, 1);
  }
  else if(m == 200) {
    devicesVibrate(0xFF, 0xFF, 2);
  }
  else if(m == 300) {
    devicesLED(0xFF, 0xFF, 2);
  }
  else if(m == 400) {
    devicesForm(0xFF, 0xFF, 0);
  }
  else if(m == 500) {
    devicesForm(0xFF, 0xFF, 1);
  }
  else if(m == 600) {
    devicesSound(0xFF, 0xFF, 1);
  }
  else if(m == 700) {
    devicesSound(0xFF, 0xFF, 1);
  }
  
  try
  {
    devicesRead();
  }
  catch(Exception e)
  {
    e.printStackTrace();
  }
}

void devicesButtonEvent(XBeeAddress16 address)
{
  print("Button event from device ");
  println(address.get16BitValue());
}



