import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.rapplogic.xbee.api.ApiId;
import com.rapplogic.xbee.api.XBee;
import com.rapplogic.xbee.api.XBeeAddress16;
import com.rapplogic.xbee.api.XBeePacket;
import com.rapplogic.xbee.api.XBeeResponse;
import com.rapplogic.xbee.api.wpan.TxRequest16;
import com.rapplogic.xbee.api.wpan.TxRequest64;
import com.rapplogic.xbee.api.wpan.TxStatusResponse;
import com.rapplogic.xbee.api.PacketListener;

XBee devicesXBee = new XBee();
Queue<XBeeResponse> devicesQueue = new ConcurrentLinkedQueue<XBeeResponse>();
XBeeResponse devicesResponse;

// Sets up the devices
// serialPort: The serial port to open
void devicesSetup(String serialPort)
{
  try
  {
    PropertyConfigurator.configure(dataPath("log4j.properties"));
    devicesXBee.open(serialPort, 9600);
    devicesXBee.addPacketListener(new PacketListener() {
      public void processResponse(XBeeResponse response) {
        devicesQueue.offer(response);
      }
    });
  }
  catch(XBeeException e)
  {
    e.printStackTrace();
  }
}

// Sends out data using the radio.
// high: high byte of the destination address (0xFF = broadcast)
// low: low byte of the destination address (0xFF = broadcast)
void devicesSend(int high, int low, int[] bytes)
{
  try
  {
    XBeeAddress16 destination = new XBeeAddress16(high, low);
    TxRequest16 tx = new TxRequest16(destination, bytes);
    devicesXBee.sendAsynchronous(tx);
  }
  catch(XBeeException e)
  {
    e.printStackTrace();
  }
}

// Reads incoming radio data.
void devicesRead() throws Exception {
  while ((devicesResponse = devicesQueue.poll()) != null) {
     if (devicesResponse.getApiId() == ApiId.RX_16_RESPONSE) {
          RxResponse16 response = (RxResponse16)devicesResponse;
          int[] bytes = response.getProcessedPacketBytes();
          if(bytes.length > 1)
          {
            if(bytes[bytes.length - 2] == 'B')
            {
              devicesButtonEvent((XBeeAddress16)response.getSourceAddress());
            }
          }
     }
  }
}

// Sends a vibrate command
// high: high byte of the destination address (0xFF = broadcast)
// low: low byte of the destination address (0xFF = broadcast)
// effect: The vibration effect number
void devicesVibrate(int high, int low, int effect)
{
  int[] payload = {'0' + effect, 'V'};
  devicesSend(high, low, payload);
}

// Sends a LED command
// high: high byte of the destination address (0xFF = broadcast)
// low: low byte of the destination address (0xFF = broadcast)
// effect: The LED effect number
void devicesLED(int high, int low, int effect)
{
  int[] payload = {'0' + effect, 'L'};
  devicesSend(high, low, payload);
}

// Sends a change form command
// high: high byte of the destination address (0xFF = broadcast)
// low: low byte of the destination address (0xFF = broadcast)
// id: The form id
void devicesForm(int high, int low, int id)
{
  int[] payload = {'0' + id, 'F'};
  devicesSend(high, low, payload);
}

// Sends a play sound command
// high: high byte of the destination address (0xFF = broadcast)
// low: low byte of the destination address (0xFF = broadcast)
// id: The form id
void devicesSound(int high, int low, int id)
{
  int[] payload = {'0' + id, 'S'};
  devicesSend(high, low, payload);
}
