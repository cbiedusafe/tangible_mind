import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.rapplogic.xbee.api.ApiId;
import com.rapplogic.xbee.api.XBee;
import com.rapplogic.xbee.api.XBeeAddress16;
import com.rapplogic.xbee.api.XBeePacket;
import com.rapplogic.xbee.api.XBeeResponse;
import com.rapplogic.xbee.api.wpan.TxRequest16;
import com.rapplogic.xbee.api.wpan.TxStatusResponse;
import com.rapplogic.xbee.api.PacketListener;

XBee devicesXBee = new XBee();
Queue<XBeeResponse> devicesQueue = new ConcurrentLinkedQueue<XBeeResponse>();
XBeeResponse devicesResponse;

// Sets up the devices
// serialPort: The serial port to open
void devicesSetup(String serialPort)
{
  try
  {
    PropertyConfigurator.configure(dataPath("log4j.properties"));
    devicesXBee.open(serialPort, 9600);
    devicesXBee.addPacketListener(new PacketListener() {
      public void processResponse(XBeeResponse response) {
        devicesQueue.offer(response);
      }
    });
  }
  catch(XBeeException e)
  {
    e.printStackTrace();
  }
}

// Sends out data using the radio
// destination: The address of the endpoint
// bytes: The bytes of data to send
void devicesSend(XBeeAddress16 destination, int[] bytes)
{
  try
  {
    TxRequest16 tx = new TxRequest16(destination, bytes);
    devicesXBee.sendAsynchronous(tx);
  }
  catch(XBeeException e)
  {
    e.printStackTrace();
  }
}

// Reads incoming radio data. You need to define a callback function:
// void devicesButtonEvent(XBeeAddress16 address)
void devicesRead() {
  try
  {
    while ((devicesResponse = devicesQueue.poll()) != null) { 
      if (devicesResponse.getApiId() == ApiId.RX_16_RESPONSE) {
        RxResponse16 response = (RxResponse16)devicesResponse;
        int[] bytes = response.getProcessedPacketBytes();
        if(bytes.length > 1)
        {
          if(bytes[bytes.length - 2] == 'B')
          {
            devicesButtonEvent((XBeeAddress16)response.getSourceAddress());
          }
        }
      }
    }
  }
  catch(Exception e)
  {
    e.printStackTrace();
  }
}

// Sends a vibrate command
// destination: The address of the endpoint
// effect: The vibration effect number
void devicesVibrate(XBeeAddress16 destination, int effect)
{
  int[] payload = {'0' + effect, 'V'};
  devicesSend(destination, payload);
}

// Sends a LED command
// destination: The address of the endpoint
// effect: The LED effect number
void devicesLED(XBeeAddress16 destination, int effect)
{
  int[] payload = {'0' + effect, 'L'};
  devicesSend(destination, payload);
}

// Sends a change form command
// destination: The address of the endpoint
// id: The form id
void devicesForm(XBeeAddress16 destination, int id)
{
  int[] payload = {'0' + id, 'F'};
  devicesSend(destination, payload);
}

// Sends a play sound command
// destination: The address of the endpoint
// id: The form id
void devicesSound(XBeeAddress16 destination, int id)
{
  int[] payload = {'0' + id, 'S'};
  devicesSend(destination, payload);
}

// Catches the button press events
// address: The address of the endpoint
void devicesButtonEvent(XBeeAddress16 address)
{
  if(address == devicesPlayer1)
  {
    if(player1Pos != null)
    {
      int iconIndex = checkIcons(player1Pos, hiddenIcons1);
      if(iconIndex >= 0)
      {
        int z = (int)hiddenIcons1.get(iconIndex).z;        
        if(iconIndex == 0)
        {
          foundIcons.add(z);
          hiddenIcons1.remove(iconIndex);
        }
        else
        {
          devicesForm(devicesPlayer1, z);
        }
      }
    }
  }
  else if(address == devicesPlayer2)
  {
    if(player2Pos != null)
    {
      int iconIndex = checkIcons(player2Pos, hiddenIcons2);
      if(iconIndex >= 0)
      {
        int z = (int)hiddenIcons2.get(iconIndex).z;
        if(iconIndex == 0)
        {
          foundIcons.add(z);
          hiddenIcons2.remove(iconIndex);
        }
        else
        {
          devicesForm(devicesPlayer2, z);
        }
      }
    }
  }

  print("Button down ");
  println(address);
}



