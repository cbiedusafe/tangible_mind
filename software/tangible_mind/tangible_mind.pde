import java.util.*;

PFont font;
PImage img;
PImage player1Area;
PImage player2Area;

int score = 0;

void setup()
{
  size(1280,800);
  noStroke();
  fill(0); 
  
  devicesSetup(devicesSerialPort);
  setupTuio();
  setupIcons();  
 
  font = createFont("Arial", 18);
 
  img = loadImage("../media/brain_bg_1280.png");
  player1Cursor = loadImage("../media/1.png");
  player2Cursor = loadImage("../media/5.png");
  player1Area = loadImage("../media/player1_area.png");
  player2Area = loadImage("../media/player2_area.png");
  
  loop();
  frameRate(30);
  //noLoop();
}  

/* Uncomment to Toggle fullscreen
 boolean sketchFullScreen() {
  return true;
}
*/

// within the draw method we retrieve a Vector (List) of TuioObject and TuioCursor (polling)
// from the TuioProcessing client and then loop over both lists to draw the graphical feedback.



void draw()
{
  devicesRead();
  updateTuio();
  
  if(!activityRunning)
  {
    if(playersCollided())
    {
      activityStart();
    }
  }
  else
  {
    activityUpdate();
    activityTryEnd();
  }
  

    
  background(img);
  resetMatrix();

  textFont(font,18*scale_factor);
  float obj_size = object_size*scale_factor; 
  float cur_size = cursor_size*scale_factor; 
   
  drawPlayers();    
  drawIcons(hiddenIcons1);
  drawIcons(hiddenIcons2);
  
}
