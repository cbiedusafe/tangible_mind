import TUIO.*;
import java.util.*;

  // Icon stack
PImage[] iconURL = new PImage[8];
  // ArrayList of points of icons
ArrayList<PVector> hiddenIcons1 = new ArrayList<PVector>();
ArrayList<PVector> hiddenIcons2 = new ArrayList<PVector>();

ArrayList<PVector> visibleIcons1 = new ArrayList<PVector>();
ArrayList<PVector> visibleIcons2 = new ArrayList<PVector>();

ArrayList<Integer> foundIcons = new ArrayList<Integer>();

void setupIcons()
{      
  for (int i = 0; i < 8; i++)
  {
    iconURL[i] =  loadImage("../media/"+(i+1)+".png");
  }
}

void randomizeIcons()
{
  while (hiddenIcons1.size() < numberOfHiddenIcons)
  {
    PVector icon = generateIcon();
    
    if(iconTooClose(icon, hiddenIcons1))
      continue;
    
    println(player1Area.get((int)icon.x, (int)icon.y));
    
    println(brightness(player1Area.get((int)icon.x, (int)icon.y)));
     // continue;
      
    hiddenIcons1.add(icon);
  }
  while (hiddenIcons2.size() < numberOfHiddenIcons)
  {
    PVector icon = generateIcon();
    
    if(iconTooClose(icon, hiddenIcons2))
      continue;
      
    println(brightness(player2Area.get((int)icon.x, (int)icon.y)));
      //continue;
      
    hiddenIcons2.add(icon);
  }
}

int whichIcon(PVector position, List<PVector> icons)
{
  for (int j = 0; j < icons.size(); j++)
  {
    if(abs(position.x - icons.get(j).x) < iconSize && abs(position.y - icons.get(j).y) < iconSize)
    {
      return (int)icons.get(j).z;
    }
  }
  return -1;
}

int checkIcons(PVector position, List<PVector> icons)
{
  for (int j = 0; j < icons.size(); j++)
  {
    if(abs(position.x- icons.get(j).x) < iconSize && abs(position.y - icons.get(j).y) < iconSize)
    {
       return j;
    }
  }
  return -1;
}


void drawIcons(List<PVector> icons) 
{
  for(int j = 0; j < icons.size(); j++)
  {
     pushMatrix();
     translate(icons.get(j).x, icons.get(j).y);
     //scale(0.2);
     for(int k = 0; k < icons.size(); k++)
     {    
       image(iconURL[int(icons.get(j).z)], -487/2, -413/2);
     }
     popMatrix();
   }
}

boolean iconTooClose(PVector icon, List<PVector> occupied)
{
  for(int i = 0; i < occupied.size(); ++i)
  {
    if(abs(icon.x - occupied.get(i).x) < iconSize || abs(icon.x - occupied.get(i).y) < iconSize)
      return true;
  }
  return false;
}

PVector generateIcon() 
{
  int localx = int(random(0,width)); 
  int localy = int(random(0,height));  
  int iconNbr = int(random(1,iconURL.length));
 
  PVector pvector = new PVector(localx,localy,iconNbr);
  return pvector;
} 


