PImage player1Cursor;
PImage player2Cursor;
PVector player1Pos;
PVector player2Pos;
float player1Angle = 0;
float player2Angle = 0;
boolean player1OnIcon = false;
boolean player2OnIcon = false;
int player1Icon = 0;
int player2Icon = 0;

void setupPlayers()
{
}

boolean playersCollided()
{
  if(player1Pos != null && player2Pos != null)
  {
    return player1Pos.dist(player2Pos) < playerCollisionDistance;
  }
  return false;
}

void drawPlayers()
{  
  if(player1Pos != null)
  {
    int iconIndex = whichIcon(player1Pos, hiddenIcons1);
    if(iconIndex >= 0)
    {
      if(!player1OnIcon)
      {
        player1OnIcon = true;
        devicesVibrate(devicesPlayer1, playerOnIconVibrationEffect);
      }
    }
    else
      player1OnIcon = false;
    
    pushMatrix();
    translate(player1Pos.x, player1Pos.y);
    rotate(-player1Angle);   
    fill(0, 255, 128);
    ellipse(0, 0, iconSize, iconSize);
    popMatrix();
  }
  if(player2Pos != null)
  {
    int iconIndex = whichIcon(player1Pos, hiddenIcons1);
    if(iconIndex >= 0)
    {
      if(!player2OnIcon)
      {
        player2OnIcon = true;
        devicesVibrate(devicesPlayer1, playerOnIconVibrationEffect);
      }
    }
    else
      player2OnIcon = false;
    
    pushMatrix();
    translate(player2Pos.x, player2Pos.y);
    rotate(-player2Angle);
    fill(255, 0, 255, 128);
    ellipse(0, 0, iconSize, iconSize);
    popMatrix();
  }
}
