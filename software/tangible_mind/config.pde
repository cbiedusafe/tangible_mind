// XBee serial port
String devicesSerialPort = "COM18";

// Player device addresses
XBeeAddress16 devicesPlayer1 = new XBeeAddress16(0x00, 0x02);
XBeeAddress16 devicesPlayer2 = new XBeeAddress16(0x00, 0x03);

// Broadcast address
XBeeAddress16 devicesBroadcast = new XBeeAddress16(0xFF, 0xFF);

// Player TUIO symbol IDs
int tuioPlayer1 = 0;
int tuioPlayer2 = 0;

int numberOfHiddenIcons = 6;

int iconSize = 100;
int iconSize2 = iconSize * iconSize;

int playerOnIconVibrationEffect = 2;
float playerCollisionDistance = 20;

PVector playerOffset1 = new PVector(0, 0, 0);
PVector playerOffset2 = new PVector(0, 0, 0);
