boolean activityRunning = false;

// Starts the activity
void activityStart()
{
  activityRunning = true;
  score = 0;
  randomizeIcons();
  devicesVibrate(devicesBroadcast, 1);
  devicesLED(devicesBroadcast, 1);
}

boolean collision = true;
void activityUpdate()
{
  // Swap icons
  if(playersCollided() && !collision)
  {
    collision = true;
    int tmp = player1Icon;
    player1Icon = player2Icon;
    player2Icon = tmp;
  }
  else
    collision = false;
}

// Checks if the activity is finished and acts accordingly
void activityTryEnd()
{
  if(hiddenIcons1.size() == 0 && hiddenIcons2.size() == 0)
  {
    devicesVibrate(devicesBroadcast, 1);
    devicesLED(devicesBroadcast, 1);
    activityRunning = false;
  }
}

