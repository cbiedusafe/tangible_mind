import TUIO.*;
import java.util.*;
TuioProcessing tuioClient;

// these are some helper variables which are used
// to create scalable graphical feedback
float cursor_size = 15;
float object_size = 60;
float table_size = 760;
float scale_factor = 1;

void setupTuio()
{
  scale_factor = height / table_size;
  tuioClient  = new TuioProcessing(this);
}

// within the draw method we retrieve a Vector (List) of TuioObject and TuioCursor (polling)
// from the TuioProcessing client and then loop over both lists to draw the graphical feedback.
void updateTuio()
{
}

// these callback methods are called whenever a TUIO event occurs

// called when an object is added to the scene
void addTuioObject(TuioObject tobj)
{
  if(tuioPlayer1 == 0)
  {
    tuioPlayer1 = tobj.getSymbolID();
  }
  else if(tuioPlayer2 == 0 && tobj.getSymbolID() != tuioPlayer1)
  {
    tuioPlayer2 = tobj.getSymbolID();
  }
  
  if(tobj.getSymbolID() == tuioPlayer1)
  {
    player1Pos = new PVector(tobj.getScreenX(width), tobj.getScreenY(height), 0.0);
    player1Angle = tobj.getAngle();
    PVector offset = playerOffset1;
    offset.rotate(player1Angle);
    player1Pos.add(offset);
  }
  else if(tobj.getSymbolID() == tuioPlayer2)
  {
    player2Pos = new PVector(tobj.getScreenX(width), tobj.getScreenY(height), 0.0);
    player2Angle = tobj.getAngle();
    PVector offset = playerOffset2;
    offset.rotate(player2Angle);
    player2Pos.add(offset);
  }
  
  println("add object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle());
}

// called when an object is removed from the scene
void removeTuioObject(TuioObject tobj)
{
  if(tobj.getSymbolID() == tuioPlayer1)
  {
    player1Pos = new PVector(-1000, -1000, -1000);
  }
  else if(tobj.getSymbolID() == tuioPlayer2)
  {
    player2Pos = new PVector(1000, 1000, 1000);
  }
  
  println("remove object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+")");
}

// called when an object is moved
void updateTuioObject (TuioObject tobj)
{
  if(tobj.getSymbolID() == tuioPlayer1)
  {
    if(player1Pos != null)
    {
      player1Pos.x = tobj.getScreenX(width);
      player1Pos.y = tobj.getScreenY(height);
      player1Angle = tobj.getAngle();
      PVector offset = playerOffset1;
      offset.rotate(player1Angle);
      player1Pos.add(offset);
    }
    
  }
  else if(tobj.getSymbolID() == tuioPlayer2)
  {
    if(player2Pos != null)
    {
      player2Pos.x = tobj.getScreenX(width);
      player2Pos.y = tobj.getScreenY(height);
      player2Angle = tobj.getAngle();
      PVector offset = playerOffset2;
      offset.rotate(player2Angle);
      player2Pos.add(offset);
    }
  }
  
  //println("update object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle()
   //       +" "+tobj.getMotionSpeed()+" "+tobj.getRotationSpeed()+" "+tobj.getMotionAccel()+" "+tobj.getRotationAccel());
}

// called when a cursor is added to the scene
void addTuioCursor(TuioCursor tcur) {
  //println("add cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY());
}

// called when a cursor is moved
void updateTuioCursor (TuioCursor tcur) {
  //println("update cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY()+" "+tcur.getMotionSpeed()+" "+tcur.getMotionAccel());
}

// called when a cursor is removed from the scene
void removeTuioCursor(TuioCursor tcur) {
  //println("remove cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+")");
}

// called after each message bundle
// representing the end of an image frame
void refresh(TuioTime bundleTime) { 
  redraw();
}
