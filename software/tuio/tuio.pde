/*
    TUIO processing demo - part of the reacTIVision project
    http://reactivision.sourceforge.net/

    Copyright (c) 2005-2009 Martin Kaltenbrunner <mkalten@iua.upf.edu>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

// we need to import the TUIO library
// and declare a TuioProcessing client variable
import TUIO.*;
import java.util.*;
TuioProcessing tuioClient;

// these are some helper variables which are used
// to create scalable graphical feedback
float cursor_size = 15;
float object_size = 60;
float table_size = 760;
float scale_factor = 1;
PFont font;
PImage img;
PImage player1Cursor;
PImage player2Cursor;
PImage[] iconURL = new PImage[9];



  // ArrayList of points of icons
  ArrayList<PVector> hiddenIcons = new ArrayList<PVector>();
  ArrayList<PVector> foundIcons = new ArrayList<PVector>();


void setup()
{
  //size(screen.width,screen.height);
  size(1280,800);
  noStroke();
  fill(0);
  

  
  while (hiddenIcons.size() < 5)
  {
  hiddenIcons.add(generateNewPVectorToList());
  println(hiddenIcons.get(hiddenIcons.size()-1));
  
  }
  println(hiddenIcons);
  
  
  
  loop();
  frameRate(30);
  //noLoop();
  
 
  font = createFont("Arial", 18);
  scale_factor = height/table_size;
  
  img = loadImage("../media/brain_bg_1280.png");
  player1Cursor = loadImage("../media/1.png");
  player2Cursor = loadImage("../media/5.png");
  
  for (int i = 0;i<9;i++) {
  iconURL[i] =  loadImage("../media/"+(i+1)+".png");
  }  

  
  // we create an instance of the TuioProcessing client
  // since we add "this" class as an argument the TuioProcessing class expects
  // an implementation of the TUIO callback methods (see below)
  tuioClient  = new TuioProcessing(this);
  
  
  
  
}

/* Uncomment to Toggle fullscreen
 boolean sketchFullScreen() {
  return true;
}
*/

// within the draw method we retrieve a Vector (List) of TuioObject and TuioCursor (polling)
// from the TuioProcessing client and then loop over both lists to draw the graphical feedback.


PVector generateNewPVectorToList () 
{
int localx = int(random(0,width)); 
int localy = int(random(0,height));
 
  PVector pvector = new PVector(localx,localy);

  return pvector;
} 

void checkSymbols()
{

  Vector tuioObjectList = tuioClient.getTuioObjects();
  
  for (int i=0;i<tuioObjectList.size();i++) {
     
    TuioObject tobj = (TuioObject)tuioObjectList.elementAt(i);
     
     for (int j=0;j<hiddenIcons.size();j++) {
       if(abs(tobj.getScreenX(width) - hiddenIcons.get(j).x) <30 && abs(tobj.getScreenY(height) - hiddenIcons.get(j).y) <30)
         {
        foundIcons.add(hiddenIcons.remove(j));
         }
     }
  
  } 
}


void draw()
{
  checkSymbols();
  /*
  if(hiddenIcons != null) 
  println(hiddenIcons);
    
   if(foundIcons != null) 
    println(foundIcons);
    */
    
  scale(width/img.width, height/img.height);
  background(img);
  resetMatrix();

  textFont(font,18*scale_factor);
  float obj_size = object_size*scale_factor; 
  float cur_size = cursor_size*scale_factor; 
   
  Vector tuioObjectList = tuioClient.getTuioObjects();

  for (int i=0;i<tuioObjectList.size();i++) {
     TuioObject tobj = (TuioObject)tuioObjectList.elementAt(i);
  
     pushMatrix();
     translate(tobj.getScreenX(width),tobj.getScreenY(height));
     rotate(tobj.getAngle());
     scale(0.2);
    //rect(-obj_size/2,-obj_size/2,obj_size,obj_size);
    if(tobj.getSymbolID() == 115) {
    image(player1Cursor, -487/2, -413/2);
    }
    if(tobj.getSymbolID() == 116) {
    image(player2Cursor, -461/2, -278/2);
    }
    

     popMatrix();
     
     
     
     

      
      for (int j=0;j < hiddenIcons.size();j++)
      {
        pushMatrix();
        
        
       translate(hiddenIcons.get(j).x,hiddenIcons.get(j).y);
       scale(0.2);
       for(int k=0;k < hiddenIcons.size();k++) {
          image(iconURL[k], -487/2, -413/2);
       }
       popMatrix();
 
      }

     
     

     
       
     
     // text(""+tobj.getSymbolID(), tobj.getScreenX(width), tobj.getScreenY(height));
   }
 
 /*  
   Vector tuioCursorList = tuioClient.getTuioCursors();
   for (int i=0;i<tuioCursorList.size();i++) {
      TuioCursor tcur = (TuioCursor)tuioCursorList.elementAt(i);
      Vector pointList = tcur.getPath();
      
      if (pointList.size()>0) {
        stroke(0,0,255);
        TuioPoint start_point = (TuioPoint)pointList.firstElement();;
        for (int j=0;j<pointList.size();j++) {
           TuioPoint end_point = (TuioPoint)pointList.elementAt(j);
           line(start_point.getScreenX(width),start_point.getScreenY(height),end_point.getScreenX(width),end_point.getScreenY(height));
           start_point = end_point;
        }
        
        
        stroke(192,192,192);
        fill(192,192,192);
        ellipse( tcur.getScreenX(width), tcur.getScreenY(height),cur_size,cur_size);
        fill(0);
        text(""+ tcur.getCursorID(),  tcur.getScreenX(width)-5,  tcur.getScreenY(height)+5);
      }
   }
   */
}

// these callback methods are called whenever a TUIO event occurs

// called when an object is added to the scene
void addTuioObject(TuioObject tobj) {
  println("add object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle());
}

// called when an object is removed from the scene
void removeTuioObject(TuioObject tobj) {
  println("remove object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+")");
}

// called when an object is moved
void updateTuioObject (TuioObject tobj) {
  println("update object "+tobj.getSymbolID()+" ("+tobj.getSessionID()+") "+tobj.getX()+" "+tobj.getY()+" "+tobj.getAngle()
          +" "+tobj.getMotionSpeed()+" "+tobj.getRotationSpeed()+" "+tobj.getMotionAccel()+" "+tobj.getRotationAccel());

}

// called when a cursor is added to the scene
void addTuioCursor(TuioCursor tcur) {
  println("add cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY());
}

// called when a cursor is moved
void updateTuioCursor (TuioCursor tcur) {
  println("update cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+ ") " +tcur.getX()+" "+tcur.getY()
          +" "+tcur.getMotionSpeed()+" "+tcur.getMotionAccel());
}

// called when a cursor is removed from the scene
void removeTuioCursor(TuioCursor tcur) {
  println("remove cursor "+tcur.getCursorID()+" ("+tcur.getSessionID()+")");
}

// called after each message bundle
// representing the end of an image frame
void refresh(TuioTime bundleTime) { 
  redraw();
}
